FROM docker.io/oven/bun:1 AS base
FROM docker.io/mikefarah/yq AS yq
FROM base AS install

COPY . /app
COPY --from=yq /usr/bin/yq /bin/yq

WORKDIR /app

RUN bun install

RUN <<EOS
set -e
cat > /bin/vtt-entrypoint.sh <<EOF
#!/usr/bin/env -S sh -e

cp config.template.json config.json
for key in \$(yq -otsv 'keys' config.json); do
    [ -n "\$(eval echo \\$\$key)" ] && \
        yq -i -oj ".\$key = strenv(\$key)" \
        config.json
done

cat config.json
set -x

"\$@"
EOF
chmod +x /bin/vtt-entrypoint.sh
EOS

ENTRYPOINT [ "/bin/vtt-entrypoint.sh" ]
CMD [ "bun", "start" ]
