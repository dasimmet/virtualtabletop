# virtualtabletop

This project builds a container image of virtualtabletop.

You can host your own instance from the [Container Registry](https://gitlab.com/dasimmet/virtualtabletop/container_registry)
or go to [VirtualTabletop.io](https://virtualtabletop.io).

The Repository is mirrored from [Github](https://github.com/ArnoldSmith86/virtualtabletop).
